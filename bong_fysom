#!/usr/bin/python3
"""
 bong
    a ping wrapper

 It looks for lines matching the reqular expression
    .*icmp_.eq=(.*) ttl=.*
 to detect
   - still_up
   - down
   - up_again


 Typical use case is waiting for a server that is being rebooted.

 Note that a single lost packet is seen as an entire reboot.


 Geert Stappers  2021-10-14   GPLv2


    bong
    Copyright (C) 2018-2021  Hendrikx-ITC

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; version 2 of the License.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

"""

import datetime
import os
import pty
import re
import signal
import subprocess
import sys
import time


if len(sys.argv) < 2:
    sys.stderr.write("E: Missing hostname\n")
    sys.stderr.write("I: This programm reduces output of `ping`\n")
    sys.stderr.write("I: Like `ping` is a hostname as parameter required\n")
    sys.exit(1)

try:
    from fysom import Fysom
except ImportError:
    sys.stderr.write("E: importing FySoM\n")
    sys.stderr.write("I: apt install python3-fysom\n")
    sys.exit(1)

icmpreply = re.compile(r".*icmp_.eq=(.*) ttl=.*")

proceed = True

lsl = ""  # last seen line
ec = 255  # exit code

debug_output_wanted = True
debug_output_wanted = False


def debug_output(string):
    if debug_output_wanted:
        print(string)


def control_C_handler(sig, frame):
    """for exit on Control-C"""
    global proceed
    global ec
    if proceed is False:
        # Most likely Control-C has already been pressed
        sys.stderr.write(" I: Doing hasty closure\n")
        # No attempt is done to stop the subprocess
        # or any other cleanup.
        sys.exit(1)
    else:
        # main loop shall stop the subprocess
        proceed = False
        ec = 130


def control_backslash_handler(sig, frame):
    """dump FSM state, seqnum found, last seen input"""
    global lsl
    dump = "%s %s" % (fsm.current, lsl)
    sys.stdout.write(dump)


fsm = Fysom(
    {
        "initial": "just_started",
        "events": [
            {"name": "next_sqn", "src": "just_started", "dst": "still_up"},
            {"name": "rise", "src": "down", "dst": "up_again"},
            {"name": "fall", "src": ["just_started", "still_up"], "dst": "down"},
            {
                "name": "leap_sqn",
                "src": ["just_started", "still_up", "down"],
                "dst": "up_again",
            },
        ],
    }
)

if os.getenv("BONG_COMMAND"):
    debug_output("DBG setting command through BONG_COMMAND environment var.")
    command = os.getenv("BONG_COMMAND") % sys.argv[1]
else:
    command = "ping %s" % sys.argv[1]
debug_output("DBG command %s" % command)


def get_seq_num(line):
    gotreply = icmpreply.match(line)
    if gotreply:
        return int(gotreply.group(1))
    else:
        return 0


def info_line():
    global ts_prv
    ts_now = datetime.datetime.now().replace(microsecond=0)
    hms = ts_now.strftime("%H:%M:%S ")
    delta = str(ts_now - ts_prv)
    ts_prv = ts_now
    sys.stdout.write(hms + fsm.current + " " + delta + "\n")
    sys.stdout.flush()


#
# Now start executing
ts_prv = datetime.datetime.now().replace(microsecond=0)
info_line()
master_pty, slave_pty = pty.openpty()
process = subprocess.Popen(
    command.split(),
    stdout=slave_pty,
    stderr=sys.stderr,
)
p_stdout = os.fdopen(master_pty)

time.sleep(1)  # To give process some headstart plus time to fail.
#                So the first process.poll() can see a aborted start.
#           Such as when ping can't resolve a hostname and instantly stops.

signal.signal(signal.SIGINT, control_C_handler)
signal.signal(signal.SIGQUIT, control_backslash_handler)

if process.poll() is None:
    line = p_stdout.readline()
    if len(line) > 0:
        print(line.rstrip())
else:
    sys.stderr.write("E: subprocess stopt")
    sys.stderr.write(" before first output line\n")
    sys.stderr.write("I: subprocess exitcode %s\n" % str(process.returncode))
    sys.exit(1)

esqn = 1  # expected sequence number
while proceed and process.poll() is None:
    line = p_stdout.readline()
    # NOTE Waiting (forever) for subprocess output happens in above line ...
    if len(line) > 0:
        sqn = get_seq_num(line)
        debug_output("DBG expect SQN %d and got Sequence Number %d" % (esqn, sqn))
        lsl = "%d %s" % (sqn, line)  # last seen line for CTRL-\ dump
        if sqn == 0 and not fsm.isstate("down"):
            fsm.fall()
            info_line()
        if sqn > esqn:
            fsm.leap_sqn()
        if fsm.isstate("still_up") and (sqn == esqn):
            esqn += 1
        if fsm.isstate("just_started") and (sqn == esqn):
            fsm.next_sqn()
            info_line()
            esqn += 1
        if fsm.isstate("down") and (sqn > 0):
            fsm.rise()
        if fsm.isstate("up_again"):
            proceed = False
            ec = 0
            process.send_signal(signal.SIGHUP)
        _ = process.poll()  # to update the subprocess returncode
        debug_output("DBG subprocess returncode %s" % str(process.returncode))
        debug_output("DBG current state %s" % fsm.current)

info_line()

if proceed is True:
    # the subprocess has stopped, but it should have proceed
    _ = process.poll()  # to update the subprocess returncode
    ec = int(str(process.returncode))

if ec != 0:
    sys.stderr.write("E: Stopping with non-zero exit code %d\n" % ec)

sys.exit(ec)

# l l
